const React = require("react");

function DefaultLayout(props) {
  return (
    <html lang="en">
      <head>
        <meta charset="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        />
        <title>{props.title}</title>
      </head>
      <body>{props.children}</body>
    </html>
  );
}

module.exports = DefaultLayout;
