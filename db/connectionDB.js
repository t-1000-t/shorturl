const mongoose = require("mongoose");
const { mongodbUri } = require("../config/config");

const option = {
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
  useNewUrlParser: true
};

async function connectionDB() {
  try {
    await mongoose.connect(mongodbUri, option);
    console.log("MongoDB connected...");
  } catch (error) {
    console.log("MongodB connection error :", error);
    process.exit(1);
  }
}

module.exports = connectionDB;
